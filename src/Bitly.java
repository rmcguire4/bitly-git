import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Bitly extends Application {
	
	//initialize variables
	private final String accessToken = "7a10b2c3c064f0247f7005d5a6e2c301e3ee87f7";
	String longUrl, shortUrl;
	
	//initialize gui items
	Button btnsubmit;
	TextField input, output;
	
	@Override
	public void start(Stage primaryStage) {
		//create controls
		input = new TextField("Enter URL");
		btnsubmit = new Button("Submit");
		output = new TextField("Short URL");
		
		//center align text
		input.setAlignment(Pos.CENTER);
		btnsubmit.setAlignment(Pos.CENTER);
		output.setAlignment(Pos.CENTER);
	    		
		//make container for app
		VBox root = new VBox();
		//put in center scene
		root.setAlignment(Pos.CENTER);
	    // set spacing
	    root.setSpacing(15);
	    
	    //add items to VBox
	    root.getChildren().add(input);
	    root.getChildren().add(output);
	    root.getChildren().add(btnsubmit);	    
	    //setWidths
	    setWidths();
	    //attach code
	    attachCode();
	    Scene scene = new Scene(root, 350, 250);
	    primaryStage.setTitle("Ryan McGuire");
	    primaryStage.setScene(scene);
	    primaryStage.show();
		
	
	
	} //end start
	
	//local method to set width
	public void setWidths() {
		 // Set widths of all controls
		 input.setMaxWidth(300.0);
	     output.setMaxWidth(300.0);
	     btnsubmit.setPrefWidth(100.0);
	} //end setWidths
	
	//local method to attach code to buttons
	public void attachCode() {
		  // Attach actions to buttons
		  btnsubmit.setOnAction(e -> btnSubmit(e));
	} //end attach code
	
	//btnCalc method
	public void btnCalc(ActionEvent e) {
		
	}
		
	//method for submit button
		public void btnSubmit(ActionEvent e) {
			try {
				//construct an API URL with input
				longUrl = input.getText().trim();
				URL bitURL = new URL("https://api-ssl.bitly.com/v3/shorten?access_token="
						+ accessToken + "&longUrl=" + longUrl + "&format=json");		
				//open the url				
				InputStream is = bitURL.openStream(); //throws IOException?
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				
				//read the result into JSON element
				JsonElement jse = new JsonParser().parse(br);
				
				//close the connection
				is.close();
				br.close();
		
				//check if we recieved a response
				if(jse != null) {
					String status = jse.getAsJsonObject().get("status_txt").getAsString();
					if (status.equals("OK")) {
						shortUrl = jse.getAsJsonObject().get("data").getAsJsonObject().get("url").getAsString();
						output.setText(shortUrl);
					}
					else {
						output.setText(status);
					}
					
				}				
				
			} catch (Exception x) {
				output.setText(x.toString());
			}
			
		} //end btnSubmit
				
	public static void main(String[] args) {
	    launch(args);
	  }

} //end Bitly
